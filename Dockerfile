FROM python:3.10-slim-buster

LABEL Name="Python Flask Demo App" Version=1.4.2
LABEL org.opencontainers.image.source = "https://github.com/benc-uk/python-demoapp"

ARG srcDir=src
WORKDIR /app
COPY $srcDir/requirements.txt /app
RUN apt-get update && apt-get install make && apt-get install python-dev -y && apt-get install python3-dev -y && apt-get install gcc -y
RUN pip install --upgrade pip
RUN pip install -Ur /app/requirements.txt

COPY $srcDir/run.py .
COPY $srcDir/app ./app

EXPOSE 5000

CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:app"]